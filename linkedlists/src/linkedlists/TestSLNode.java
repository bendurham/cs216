package linkedlists;

public class TestSLNode {
	public static void main(String[] args) {
		Integer x1 = new Integer(2);
		Integer x2 = new Integer(5);
		
		SLNode<Integer> node1 = new SLNode<Integer>(x1);
		SLNode<Integer> node2 = new SLNode<Integer>(x2);
		
		node1.setNext(node2);
		
		System.out.println(node1.getData());
		System.out.println(node1.getNext().getData());
		
		System.out.println(node1.getNext().getNext());
	}
}
