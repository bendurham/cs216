package linkedlists;

public class TestDLList {
	public static void main(String[] args) {
		DLList<Integer> list = new DLList<Integer>();
		
		Integer int1 = new Integer(2);
		Integer int2 = new Integer(7);
		Integer int3 = new Integer(9);
		
		list.add(int1);
		list.add(int2);
		list.add(int3);
		
//		list.reverse();
		list.traverse();
	}
	
	
}
