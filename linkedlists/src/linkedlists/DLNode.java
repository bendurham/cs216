package linkedlists;

public class DLNode<T> extends SLNode<T> {
	// The reference to the preceding element
	private DLNode<T> prev;
	
	/**
	 * Creates a new DLNode containing the given data
	 * @param data the data to store in the node
	 */
	public DLNode(T data) {
		super(data);
		
		this.prev = null;
	}
	
	/**
	 * Gets the reference to the prior item
	 * @return the reference to the preceding item
	 */
	public DLNode<T> getPrev() {
		return this.prev;
	}
	
	/**
	 * Sets the reference to the prior item
	 * @param prev the new node to reference
	 */
	public void setPrev(DLNode<T> prev) {
		this.prev = prev;
	}
}
