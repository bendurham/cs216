package linkedlists;

public class TestSLList {
	public static void main(String[] args) {
		SLList<Integer> list1 = new SLList<Integer>();
		
		Integer int1 = new Integer(2);
		Integer int2 = new Integer(5);
		Integer int3 = new Integer(7);
		
		SLNode<Integer> node1 = new SLNode<Integer>(int1);
		SLNode<Integer> node2 = new SLNode<Integer>(int2);
		
		list1.add(int1);
		list1.add(int2);
		list1.add(int3);
		
		/*
		list1.traverse();
		
		System.out.println(list1.remove(5));
		list1.traverse();
		*/
		
		list1.reverse();
	}
}
