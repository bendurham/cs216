package linkedlists;

/**
 * An generic implementation of a Singly Linked list
 * 
 * @author Ben Durham
 *
 * @param <T> The type of item to store in the list
 */
public class SLList<T> {
	// Stores the current head of the list
	private SLNode<T> head;
	
	// Stores the current tail of the list
	private SLNode<T> tail;
	
	/**
	 * A basic constructor that initializes an empty SLList
	 */
	public SLList() {
		this.head = null;
		this.tail = null;
	}
	
	/**
	 * Adds an element of type T to the end of the list
	 * 
	 * @param data the item to add to the list
	 */
	public void add(T data) {
		SLNode<T> newNode = new SLNode<T>(data);
		
		/*
		 * If the list is empty, assign the new element to both the head and 
		 * tail
		 */
		if (this.head == null) {
			this.head = newNode;
			this.tail = newNode;
		} else {
			// otherwise, add it to the end of the list
			this.tail.setNext(newNode);
			tail = newNode;
		}
	}
	
	/**
	 * Removes the specified element from the list
	 * 
	 * @param data The element to remove from the list
	 * @return true if an item has been removed, false if the item is not found
	 */
	public boolean remove(T data) {
		// If list is empty
		if (this.head == null) { 
			return false;
		} else if (this.head.getNext() == null) {
			// If head is the only element
			if (this.head.getData().equals(data)) {
				// Delete the head if it matches criteria
				this.head = null;
				this.tail = null;
				
				return true;
			} else {
				return false;
			}
		} else {
			// If list has more than one element
			// And the head is the item to delete
			if (this.head.getData().equals(data)) {
				this.head = this.head.getNext();
				
				return true;
			} else {
				// Some element other than the head must be deleted (if it exists)
				SLNode<T> currNode = this.head;
				
				// Traverse the list until reaching the tail or reaching the element to delete
				while (currNode.getNext() != null && !currNode.getNext().getData().equals(data)) {
					currNode = currNode.getNext();
				}
				
				// If the element found isn't null (end of list) and matches criteria, delete
				if (currNode.getNext() != null && currNode.getNext().getData().equals(data)) {
					currNode.setNext(currNode.getNext().getNext());
					return true;
				}
				return false;
			}
		}
	}
	
	/**
	 * Traverses the list from head to tail, printing each element to the console
	 */
	public void traverse() {
		SLNode<T> currNode = this.head;
		
		System.out.println(currNode.getData());
		
		while (currNode != null) {
			System.out.println(currNode.getData());
			
			currNode = currNode.getNext();
		}
	}
	
	/**
	 * Traverses the list from tail to head, printing each element to the console
	 */
	public void reverse() {
		// Tracks each item starting from the tail
		SLNode<T> reversingNode = this.tail;
		
		// Runs from the head to just until current reversingNode
		SLNode<T> traversingNode = this.head;
		
		if (this.tail != null) {
			reversingNode = this.tail;
			while (reversingNode != this.head) {
				// Starting each traversal at the head
				traversingNode = this.head;
				
				// Traverse towards reversingNode
				while (!traversingNode.getNext().equals(reversingNode)) {
					traversingNode = traversingNode.getNext();
				}
				
				// Once traversingNode reaches just before reversingNode, print
				System.out.println(reversingNode.getData());
				
				// Reassign reversingNode so next iteration ends one item sooner
				reversingNode = traversingNode;
			}
			
			System.out.println(reversingNode.getData());
		}
	}
}
