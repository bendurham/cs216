package linkedlists;

/**
 * 
 * @author durhbe01
 *
 * @param <T>
 */
public class SLNode<T> {
	// Data contained within node
	private T data;
	
	// Forward reference to next node
	private SLNode<T> next;
	
	/**
	 * Creates a new SLNode with the given data
	 * @param data the data to store
	 */
	public SLNode(T data) {
		this.data = data;
		this.next = null;
	}
	
	/**
	 * Accesses data in the node
	 * @return the data contained within the node
	 */
	public T getData() {
		return this.data;
	}
	
	/**
	 * Sets the reference to the next node
	 * @param next the reference to the next node
	 */
	public void setNext(SLNode<T> next) {
		this.next = next;
	}
	
	/**
	 * Get the reference to the next node
	 * @return the reference to the next node
	 */
	public SLNode<T> getNext() {
		return this.next;
	}
}
