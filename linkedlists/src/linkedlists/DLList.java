package linkedlists;

public class DLList<T> {
	private DLNode<T> head;
	private DLNode<T> tail;
	
	public DLList() {
		this.head = null;
		this.tail = null;
	}
	
	public void add(T data) {
		DLNode<T> newItem = new DLNode<T>(data);
		
		if (this.head == null) {
			this.head = newItem;
			this.tail = newItem;
		} else {
			this.tail.setNext(newItem);
			newItem.setPrev(this.tail);
			this.tail = newItem;
		}
	}
	
	
	public void traverse() {
		DLNode<T> curr = this.head;
		
		while (curr != null) {
			System.out.println(curr.getData());
			curr = curr.getNext();
		}
	}
	
	public void reverse() {
		DLNode<T> curr = this.tail;
		
		while (curr != null) {
			System.out.println(curr.getData());
			curr = curr.getPrev();
		}
	}
	
}
